/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package exercise2;

/**
 * Apache NetBeans
 * Exercise1
 * @author lauri
 */

import java.util.Scanner;

public class Mainclass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String input1, input2;
        
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Give a name to the dog: ");
        input1 = scanner.nextLine();
        Dog dog1 = new Dog(input1);
        
        System.out.print("What does a dog say: ");
        input2 = scanner.nextLine();
        dog1.speak(input2);
    }
    
}